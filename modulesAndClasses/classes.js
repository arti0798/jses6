class Person {

    constructor(name) {

        // this.name = 'LEXA'
        this.name = name
    }
    greet() {

        console.log("heloo Clark, this is "+this.name);
    }
    
}
class LEXA extends Person {

    constructor(age) {

        this.age = age;
    }
}

// let person = new Person('LEXA');
let lexa = new LEXA('LEXA');
// console.log(person);
// person.greet()
lexa.greet()

// console.log(person.__proto__ == Object.prototype)
// console.log(person.__proto__ == Object)
// console.log(person.__proto__ == Person.prototype)
// console.log(person.__proto__ === Person.prototype)