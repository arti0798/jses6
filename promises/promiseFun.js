let promises = new Promise(function(resolve, reject) {

    setTimeout(function() {

        resolve('Done!');
    }, 1500);
});

promises.then(function(value) {

    console.log(value);
})