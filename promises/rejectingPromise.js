let promises = new Promise(function(resolve, reject) {

    setTimeout(function() {

        reject('Fail!');
    }, 1500);
});

promises.then(function(value) {

    console.log(value);
}, function(error) {

    console.log(error);
});