let person = {

    names: 'Arti'
};

let handler = {

    get: function(target, name) { //terget is object and name is just a property which we try to access

            return name in target ? target[name] : 'Non existant';

        } //get trap which contain 2 argument target and name and optional 3rd arugument receiver

};


var proxy = new Proxy({}, handler); //proxy contain 2 argument one is object and next is handler(whic contains our logic)
//now i can use this Proxy as prototype

Reflect.setPrototypeOf(person, proxy); // using this proxy as prototype of person (person is not wraped with prototype of proxy)

console.log(person.names); // by making its prototype we are directly able to access our object i.e person
console.log(person.hobbies);
// console.log(proxy.names);
// console.log(proxy.age);