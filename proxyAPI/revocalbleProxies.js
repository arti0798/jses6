// we always saw the default proxy use all the time but there are also revocalbe proxy
// so Revocalble proxy can be revoked i.e after we setup such a proxy as a wrapper we can remove that functionnality(we can make sure that the proxy no longer active)

let person = {
    name: 'Jhanavi'
};

let handler = {

    get: function(target, property) {

        return Reflect.get(target, property);
    }
};

let { proxy, revoke } = Proxy.revocable(person, handler);
//revoke();     
console.log(proxy.name); // after calling revoke() this line gives error because it get revoked