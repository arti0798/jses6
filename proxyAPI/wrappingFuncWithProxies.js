function log(message) {

    console.log('Log entry created, message: ' + message);
}
log('Hello');

let handler = {

    apply: function(target, thisArg, argumentsList) { //apply from Reflect API

        if (argumentsList.length == 1) {

            return Reflect.apply(target, thisArg, argumentsList);
        }
    }
};
let proxy = new Proxy(log, handler);
proxy('Hii'); //excuting proxy as function without apply
proxy('Hii', 10); // nothing happens because it fails the condition