let person = {

    names: 'Arti'
};

let handler = {

};

let protoHandler = {

    get: function(target, name) { //terget is object and name is just a property which we try to access

            return name in target ? target[name] : 'Non existant';

        } //get trap which contain 2 argument target and name and optional 3rd arugument receiver

};

let proxy = new Proxy({}, handler); //proxy contain 2 argument one is object and next is handler(whic contains our logic)

let protoProxy = new Proxy(proxy, protoHandler);

Reflect.setPrototypeOf(person, protoProxy);

console.log(proxy.names);
console.log(proxy.age);