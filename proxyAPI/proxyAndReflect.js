let person = {

    names: 'Arti',
    age: 19
};

let handler = {

    get: function(target, name) { //terget is object and name is just a property which we try to access

        return name in target ? target[name] : 'Non existant';

    }, //get trap which contain 2 argument target and name and optional 3rd arugument receiver
    set: function(target, propetry, value) {

        if (value.length >= 2) {

            Reflect.set(target, propetry, value);
        }
    }
};

var proxy = new Proxy(person, handler); //proxy contain 2 argument one is object and next is handler(whic contains our logic)

console.log(proxy.names);

proxy.names = 'Shdjhjgu4edhgfdhg';
console.log(proxy.names);
console.log(proxy.age);