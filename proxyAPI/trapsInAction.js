let person = {

    names: 'Arti'
};

let handler = {

    get: function(target, name) { //terget is object and name is just a property which we try to access

            return name in target ? target[name] : 'Non existant';

        } //get trap which contain 2 argument target and name and optional 3rd arugument receiver

};

var proxy = new Proxy(person, handler); //proxy contain 2 argument one is object and next is handler(whic contains our logic)

console.log(proxy.names);
console.log(proxy.age);