let symbol = Symbol('debug');
console.log(symbol.toString());
console.log(typeof symbol)

let anotherSYmbol = Symbol('debug')
console.log(anotherSYmbol == symbol) //both symbol are different

let obj = {

    name: 'Bellemy',
    [symbol]: 22
}

console.log(obj)