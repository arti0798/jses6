let num1 = 20;
let num2 = -20;
let num3 = 0;
let num4 = NaN;
let num5 = 'a string';
let num6 = 3.09;
let num7 = 2.54;
let num8 = -7.8

console.log(Math.sign(num1));
console.log(Math.sign(num2));
console.log(Math.sign(num3));
console.log(Math.sign(num4));
console.log(Math.sign(num5));
console.log(Math.sign(num6));
console.log(Math.trunc(num7));
console.log(Math.floor(num7));
console.log(Math.floor(num8));
console.log(Math.trunc(num8));