let array = Array(5)
console.log(array);

let array1 = Array.of(1, 2, 3);
console.log(array1);

let newArray = Array.from(array1, val => val + 2);
console.log(newArray);
console.log(array1);

array1.fill(200)
console.log(array1)

// advance

var inventory = [

    { name: 'apples', quantity: 2 },
    { name: 'bananas', quantity: 0 },
    { name: 'cherries', quantity: 5 },
];

function findCherries(fruit) {

    return fruit.name === 'cherries';
}

console.log(inventory.find(findCherries));