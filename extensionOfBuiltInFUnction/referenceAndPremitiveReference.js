const person = {

    name: 'Heena'
};

const secondPerson = person; // not created new object just perform the assignment operater

person.name = 'Oliviya';

console.log(secondPerson); //op is Oliviya because when person get copied in secondPerson it copied the pointer not value;

//same concept is applied for array
//object and array are refernce type
// for object and array reassiging means copying the pointer not value


// new copy way
// copy in inmmutabe way
const ages = {

    age: 29
};

const secongAge = { // created new object

    ...ages //spread operater
};

ages.age = 90

console.log(secongAge);