class Obj1 {

    constructor() {

        console.log('i m in OBJ1');
        this.a = 1;
    }
}

class Obj2 {

    constructor() {

        console.log('i m in OBJ2');
        this.b = 2;
    }
}

var obj1 = new Obj1();
var obj2 = new Obj2();

var obj = Object.assign(obj1, obj2); // (obj2, obj1)
console.log(obj)

console.log(obj instanceof Obj1);
console.log(obj instanceof Obj2);

console.log(obj.__proto__ === Obj1.prototype);
console.log(obj.__proto__ === Obj2.prototype);
console.log(obj.__proto__ === Object.prototype);