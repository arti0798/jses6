let person = {

    name: 'Arti'
};

let boss = {

    boss: 'Heena'
};

console.log(person.__proto__ === Object.prototype); // true => because it is normal js Object

Object.setPrototypeOf(person, boss); //change the prototype
console.log(person.__proto__ === Object.prototype); // false => because new prototype is boss

console.log(person.__proto__ === boss);
console.log(person.name);