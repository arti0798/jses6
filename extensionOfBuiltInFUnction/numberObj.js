let number = NaN;

console.log(isNaN(number));
console.log(Number.isNaN(number));

let num = Infinity;

console.log(Number.isFinite(num));
console.log(!Number.isFinite(num));