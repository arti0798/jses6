
// let name;
// let age;
let obj = {

    name : 'ARTI',
    age : 20
};
console.log(obj);

let names= 'Heena',ages = '20';
let sobj = {

    names,
    ages
};
console.log(sobj);

let def = {

    name : 'ARTI',
    age : 20,
    greet() {

        console.log(this.name + ', ' + this.age);
    }
};
def.greet();
// console.log(obj);

let fun = {

    "name" : 'ARTI',
    age : 20,
    "greet"() {

        console.log("heloo "+ this.name + ', ' + this.age);
    }
};
fun["greet"]();

let nn = 'Khushi';
let aa = 20;
let ageField = "age";
let defs = {

    "nn" : 'ARTI',
    [ageField] : 80,
    
    "greet me"() {

        console.log(this.nn + ', ' + this.aa);
    }
};
console.log(defs[ageField]);