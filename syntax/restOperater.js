let num = [1,2,3,4,5];

function sumUp(toAdd) {

    let result = 0;
    for(let i = 0;i < toAdd.length; i++) {

        result += toAdd[i];
    }
    return result;
}
console.log(sumUp(num))
// ------------------------------------------

function ss(...toAdd) { // ... rest parameter

    let result = 0;
    for(let i = 0;i < toAdd.length; i++) {

        result += toAdd[i];
    }
    return result;
}
console.log(ss(100,10,20))