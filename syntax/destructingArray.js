let num = [1, 2, 3]

// let [c, d] = num
let [c, d, e, f, g] = num

console.log(e)
console.log(f)
console.log(c)
console.log(d)
let a = num[0]
let b = num[1]
console.log(a)
console.log(b)

let [z, ...x] = num
console.log(x)

// swap

let k = 2
let j = 6

    [j, k] = [k, j];

// let a = 123;

console.log(j)
console.log(k)