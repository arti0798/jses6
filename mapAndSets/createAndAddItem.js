let cardAce = {

    name: 'Ace of Spades'
};

let cardKing = {

    name: 'King of Clubs'
};

let deck = new Map();
deck.set('as', cardAce);
deck.set('kc', cardKing);

console.log(deck)
console.log(deck.size);

//get value by using key
console.log(deck.get('as'));

//delete method
deck.delete('as');
console.log(deck.get('as'));

//clear() => will get rid of all key value pair
deck.clear();
console.log(deck.get('kc'));
