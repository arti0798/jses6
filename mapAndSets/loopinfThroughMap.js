let cardAce = {

    name: 'Ace of Spades'
};

// console.log(cardAce);
let cardKing = {

    name: 'King of Clubs'
};

let deck = new Map();
deck.set('as', cardAce);
deck.set('kc', cardKing);

console.log(deck.keys());

//keys method
for (key of deck.keys()) {

    console.log(key);
}

//values method
for (value of deck.values()) {

    console.log(value);
}

// get both key and value
for (entry of deck.entries()) {

    console.log(entry);
}