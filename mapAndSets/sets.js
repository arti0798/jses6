let set = new Set([1,1,1]);

// show uniqueness
for (element of set) {

    console.log(element); // 1 because its take unique value
}
console.log('-----------')
//add
set.add(2);

for (element of set) {

    console.log(element); // 2 get add because it is unique
}
console.log('-----------')
//delete
set.delete(1);
for (element of set) {

    console.log(element); 
}

console.log('-----------')
//has
console.log(set.has(1));
console.log(set.has(2));
for (element of set) {

    console.log(element); 
}

console.log('-----------')
//clear => get rid of all value
set.clear();
for (element of set) {

    console.log(element); 
}