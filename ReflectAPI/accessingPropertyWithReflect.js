class Person {

    constructor(name, age) {

        this._name = name;
        this.age = age;
    }

    get name() {

        console.log('i m in get name method');
        return this._name
    }

    set name(value) {

        this._name = value;
    }
}

//use of '_' is just for hidden or private variable or for namming convention

let mum = {

    _name: 'Mum'
};

let person = new Person('Ayushi', 20);

Reflect.set(person, 'name', 'Khushi'); //IN Person set name(field) as Khushi

console.log(Reflect.get(person, 'name')); //this reflect method is used for dynamic environment where Object or field u wann to access(which are passed dynamically)
//And take the advantage of refelect api to get the property of Object u donnt know
// After setting the getter And setter the "name" used in Reflect.get() will access "name" of 'get name()' mathod  (not of constructor)

Reflect.set(person, 'name', 'Arti', mum);

console.log(mum);

console.log(Reflect.get(person, 'name', mum)); //access person, access 'name' property but should refer to mum  (use this line without  Reflect.set(person, 'name', 'Arti', mum);)
// output will be Mum