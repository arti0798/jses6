class Person {

    constructor() {

        this.name = 'Heena';
    }
}

let person = new Person();
Person.prototype.age = 19;

let proto = {

    age: 20
};
Reflect.setPrototypeOf(person, proto); //change the prototype of person to proto

console.log(Reflect.getPrototypeOf(person));
console.log(Reflect.getPrototypeOf(person) == Person.prototype);