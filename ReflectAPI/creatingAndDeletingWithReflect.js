class Person {

    constructor(name, age) {

        this._name = name;
        this.age = age;
    }

    get name() {

        return this._name;
    }

    set name(value) {

        this._name = value;
    }
}


let mum = {

    _name: 'Mum'
}

let person = new Person('Hope', 20);

Reflect.defineProperty(person, 'hobbies', {

    writable: true,
    value: ['Sports', 'reading']
});
// we can add new property to our class
// if writable is true then we can override the value of 'value' i.e hobbies can be change to nothing
// if it is false => immutable

person.hobbies = ['Nothing']; // if we remove the writable (i.e flase )then o/p wll be => ['Sports', 'reading']
console.log(person.hobbies);

//we can delete any property by using delete keyword

delete person.age;

//Reflect also has delete property

Reflect.deleteProperty(person, 'age');
console.log(person.age);