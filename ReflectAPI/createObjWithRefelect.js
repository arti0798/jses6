class Person {

    constructor(name) {

        this.name = name;
    }
}

function TopObj() {

    this.age = 20;
}

let person = Reflect.construct(Person, ['Heena'], TopObj);
console.log(person);
console.log(person instanceof Person);
console.log(person.__proto__ == TopObj.prototype)