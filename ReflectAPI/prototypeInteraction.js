class Person {

    constructor() {

        this.name = 'Heena';
    }
}

let person = new Person();
Person.prototype.age = 19;

let proto = {

    age: 20,
    greet() {

        console.log('Helloo!')
    }
};
Reflect.setPrototypeOf(person, proto); //proto is prototype of person

Reflect.apply(person.greet, null, []); //person does not have greet method but its prototype have so person.greet() will call method of proto i.e greet becaues proto is prototype of greet
// console.log(Reflect.getPrototypeOf(person));
// console.log(Reflect.getPrototypeOf(person) == Person.prototype);