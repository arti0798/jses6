class Person {

    constructor(name, age) {

        this.name = name;
        this.age = age;
    }

    greet(prefix) {

        console.log(prefix + ' Hello, I m '+ this.name);
    }
}

let person = Reflect.construct(Person, ['Octaviya', 22]);
Reflect.apply(person.greet, person, []);
Reflect.apply(person.greet, {name : 'Bellame'}, ['...']);