class Person {

    constructor(name, age) {

        this._name = name;
        this.age = age;
    }

    get name() {

        return this._name;
    }

    set name(value) {

        this._name = value;
    }
}
//As Reflect Api has something to with meta programming (Meta programming evaluating the code at run time to identify the property of Object)

let mum = {

    _name: 'Mum'
}

let person = new Person('Hope', 20);
console.log(Reflect.ownKeys(person)); //ownKeys expect one argument i.e person(Object)
//it gives aall the property which are in constructer (only field)