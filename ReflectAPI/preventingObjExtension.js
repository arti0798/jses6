class Person {

    constructor(name, age) {

        this._name = name;
        this.age = age;
    }

    get name() {

        return this._name;
    }

    set name(value) {

        this._name = value;
    }
}


let mum = {

    _name: 'Mum'
}

let person = new Person('Hope', 20);

console.log(Reflect.isExtensible(person)); //to check if preventExtension is locked or not

Reflect.preventExtensions(person); //by using this property we can prevent adding additional property(no property can be added)

console.log(Reflect.isExtensible(person));

Reflect.defineProperty(person, 'hobbies', {

    writable: true,
    value: ['Sports', 'reading']
});


console.log(person.hobbies);